import traceback
import socket
from typing import List, Set, Dict, Tuple, Optional


class User:
    clients: List['User'] = []

    # public socket.socket client;
    # public String text;
    def __init__(self, client: socket.socket):
        self.username = None
        self.socket: socket.socket = client
        self.text = ""

    def listen(self, val) -> bytes:
        print('listening')
        return self.socket.recv(val)

    def send_feedback(self, msg: str) -> None:
        self.socket.send(msg.encode('UTF-8'))

    def send_message(self, msg: str) -> None:
        try:
            self.send_feedback(self.blankLine(0) + msg + '\r\n' + self.text)
        except OSError:
            self._disconnect()
            print(traceback.format_exc())

    def _broadcast(self, msg: str) -> None:
        for c in User.clients:
            if(c is not self):
                c.send_message(msg)
        self.send_feedback(msg)
        print(msg)

    def broadcast_msg(self):
        if not self.hasUsername():
            self.send_feedback('Please set username via the username command EG\r\nusername dudebro\r\n')
        else:
            self._broadcast('%s: %s\r\n' % (self.username, self.text.replace('\r\n', '')))
        self.clearInput()

    def hasSentCommand(self) -> bool:
        return self.text.startswith('/') and self.hasReturned()

    def hasReturned(self) -> bool:
        return self.text.endswith('\r\n')

    def getCommand(self) -> str:
        return self.text[1:].replace('\r\n', '')

    def clearInput(self) -> None:
        self.text = ""

    def isBackspacing(self) -> bool:
        return self.text.endswith('\x08')

    def blankLine(self, extra_chars: int) -> str:
        string = '\r'
        string += " " * (len(self.text) + extra_chars)
        string += '\r'
        return string

    def tryBackspace(self) -> None:
        if self.isBackspacing():
            self.text = self.text[:max(0, len(self.text) - 2)]
            string = self.blankLine(1) + self.text
            self.send_feedback(string)

    def tryCommand(self) -> None:
        command = self.getCommand()
        try:
            commands[command.split(' ')[0]](self, command)
        except KeyError:
            self.send_feedback('Unknown Command: %s\r\n' % self.text)
        self.clearInput()

    def update(self, data: bytes) -> None:
        self.text += data.decode('UTF-8')

    def _disconnect(self):
        User.clients.remove(self)

    def hasUsername(self) -> bool:
        return self.username is not None

    def getUsername(self) -> str:
        return self.username

    def setUsername(self, name: str) -> None:
        if self.username:
            self._broadcast("%s has changed their name to -> %s\r\n" % (self.username, name))
        else:
            self._broadcast("%s has joined the server!\r\n" % name)
        self.username = name


def command_exit(user: User, text: str):
    User.clients.remove(user)
    user.socket.close()
    exit()


def command_username(user: User, text: str):
    if len(text.split(' ')) > 1:
        user.setUsername(text[9:])
        # user.broadcast('%s has joined the chat!' % user.getUsername())
    else:
        user.send_feedback('proper syntax is "/username my_username"\r\n')


def command_Users(user: User, text: str):
    user.send_feedback(
        str(list(map(
                    (lambda u: u.getUsername() if u.hasUsername() else '_username_not_set_'), User.clients))) + '\r\n')


def command_MOTD(user: User, text: str):
    user.send_feedback("Welcome to the chat!\r\n")
    cmds = ""
    for x in commands.keys():
        cmds += x + ', '
    user.send_feedback('Avalible Commands: "%s"\r\n' % cmds)
    user.send_feedback('%s / %s users online!\r\n\n' % (len(User.clients), 40))  # TODO Fix hard code 40


commands = {
    'exit': command_exit,
    'username': command_username,
    'users': command_Users,
    'motd': command_MOTD,
}
