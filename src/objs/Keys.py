from cryptography.hazmat.primitives.asymmetric import rsa, ed25519, ed448
from typing import Any, Iterable, List, Dict, Tuple, Union
import os
from cryptography.hazmat.primitives.serialization import Encoding, PrivateFormat, load_pem_parameters, load_pem_private_key
from cryptography.hazmat.primitives import serialization


def _getOrMake(filename: str, gen_method, gen_args: Union[Tuple, List, Dict], bytes_method: str, bytes_args: Union[Iterable, Dict[str, Any]], load_method, load_args=None) -> Any:
    o = None
    filename = 'keys/' + filename
    if(not os.path.isdir('keys')):
        os.makedirs('keys')
    if not os.path.isfile(filename):
        o = gen_method(*gen_args)
        with open(filename, 'wb+') as file:
            method = getattr(o, bytes_method)
            if type(bytes_args) is dict:
                file.write(method(**bytes_args))
            else:
                file.write(method(*bytes_args))
    else:
        with open(filename, "rb+") as file:
            try:
                content = file.read()
                if len(content) == 0:
                    file.close()
                    os.remove(filename)
                    exit()
                if load_args:
                    o = load_method(content, *load_args)
                else:
                    o = load_method(content)
            except Exception:
                print('failed reading %s' % filename)
                file.close()
                os.remove(filename)
                o = _getOrMake(filename, gen_method, gen_args, bytes_method, bytes_args, load_method, load_args)
    return o


rsa_key: rsa.RSAPrivateKeyWithSerialization = _getOrMake('rsa.pem', rsa.generate_private_key, (65537, 2048), 'private_bytes', (
    Encoding.PEM, PrivateFormat.PKCS8, serialization.NoEncryption(),), load_pem_private_key, (None,))

ed25519_key: ed25519.Ed25519PrivateKey = _getOrMake('ed25519.pem', ed25519.Ed25519PrivateKey.generate, (), 'private_bytes', (
    Encoding.PEM, PrivateFormat.PKCS8, serialization.NoEncryption(),), load_pem_private_key, (None,))

ed448_key: ed448.Ed448PrivateKey = _getOrMake('ed448.pem', ed448.Ed448PrivateKey.generate, (), 'private_bytes', (
    Encoding.PEM, PrivateFormat.PKCS8, serialization.NoEncryption(),), load_pem_private_key, (None,))
