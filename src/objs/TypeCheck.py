

from typing import Any


class TypeCheckError(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


def checkType(obj: Any, _type):
    if(not isinstance(obj, _type)):
        raise TypeCheckError(f'Object must be {_type} not {type(obj)}')
