from enum import Enum
from typing import Any, Callable, Iterable, List, Sequence, Set, Dict, Tuple, Optional, Union
from cryptography.hazmat.primitives import ciphers
from cryptography.hazmat.primitives.asymmetric import dh
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.hmac import HMAC
from cryptography.hazmat.primitives.serialization import ParameterFormat, Encoding, PrivateFormat, load_pem_parameters, PublicFormat, load_pem_private_key
from cryptography.hazmat.primitives.asymmetric import rsa, x25519, x448, ed25519, ed448
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.ciphers import CipherAlgorithm, algorithms, modes, Cipher, aead
import os
import socket as sock
from cryptography.x509 import random_serial_number
from objs.Packets import PacketBuilder, Packet
from objs.PrivateKey import PrivateKey
from objs.User import User
from cryptography.hazmat.primitives.serialization.ssh import load_ssh_public_key
import objs.Diffies as diffies
from objs.TypeCheck import checkType
import traceback
from objs.LogGen import _logger, logger
import atexit
from objs.CipherWrapper import CipherWrapper
import json
from objs.SSHErrors import DecryptionError, InvalidClientKexKey, MessageAuthenticationCodeError, NoMatchingAlgorithms
from objs.Enums import DISCONNECT_CODE, KeyType, Stage, FakeStage
import logging
from objs import Keys
logging.basicConfig(level=logging.DEBUG)


_log: logger = _logger


def __write_log__():
    _log.write()


atexit.register(__write_log__)


class SSH_User:
    server_id_string: bytes = b'SSH-2.0-Biom4st3r pythonserver\r\n'

    server_supported_algos: Dict[str, Union[List[str], Dict[str, Any]]] = {
        'kex': {
            'curve448-sha512': (x448.X448PrivateKey.generate, hashes.SHA512(),),
            'curve25519-sha256': (x25519.X25519PrivateKey.generate, hashes.SHA256(),),
            'curve25519-sha256@libssh.org': (x25519.X25519PrivateKey.generate, hashes.SHA256(),),
            'diffie-hellman-group18-sha512': (diffies.DH_g18_para(), hashes.SHA512(),),
            'diffie-hellman-group17-sha512': (diffies.DH_g17_para(), hashes.SHA512(),),
            'diffie-hellman-group16-sha512': (diffies.DH_g16_para(), hashes.SHA512(),),
            'diffie-hellman-group15-sha512': (diffies.DH_g15_para(), hashes.SHA512(),),
            'diffie-hellman-group14-sha256': (diffies.DH_g14_para(), hashes.SHA256(),),
            'diffie-hellman-group14-sha1': (diffies.DH_g14_para(), hashes.SHA1(),),
            },
        'server-host-key': {
            'ssh-ed448': (hashes.SHAKE256(114), Keys.ed448_key,),
            'ssh-ed25519': (hashes.SHA512(), Keys.ed25519_key,),
            'rsa-sha2-512': (hashes.SHA512(), Keys.rsa_key,),
            'rsa-sha2-256': (hashes.SHA256(), Keys.rsa_key,),
            'ssh-rsa': (hashes.SHA1(), Keys.rsa_key,),  # rfc8032 Section 5.2.6
            # 'ssh-dss'
            },
        'encryptC2S': {
            'aes128-ctr': (algorithms.AES, modes.CTR, 128,),
            'aes128-cbc': (algorithms.AES, modes.CBC, 128,),
            'aes192-ctr': (algorithms.AES, modes.CTR, 192,),
            'aes192-cbc': (algorithms.AES, modes.CBC, 192,),
            'aes256-ctr': (algorithms.AES, modes.CTR, 256,),
            'aes256-cbc': (algorithms.AES, modes.CBC, 256,),
            'none': None
            },
        'encryptS2C': {  # Dict[str, Tuple[CipherAlgorm, Mode, bit_size]]
            'aes128-ctr': (algorithms.AES, modes.CTR, 128,),
            'aes128-cbc': (algorithms.AES, modes.CBC, 128,),
            'aes192-ctr': (algorithms.AES, modes.CTR, 192,),
            'aes192-cbc': (algorithms.AES, modes.CBC, 192,),
            'aes256-ctr': (algorithms.AES, modes.CTR, 256,),
            'aes256-cbc': (algorithms.AES, modes.CBC, 256,),
            'none': None
            },
        'macC2S': {
            'none': (None, None,),
            'hmac-sha2-256': (hashes.SHA256(), 32,),
            'hmac-sha1': (hashes.SHA1(), 20,),
            'hmac-md5': (hashes.MD5(), 16,),
            'hmac-sha2-512': (hashes.SHA512(), 64,),
            },
        'macS2C': {
            'none': (None, None,),
            'hmac-sha2-256': (hashes.SHA256(), 32,),
            'hmac-sha1': (hashes.SHA1(), 20,),
            'hmac-md5': (hashes.MD5(), 16,),
            'hmac-sha2-512': (hashes.SHA512(), 64,),
            },
        'compressionC2S': ['none'],
        'compressionS2C': ['none'],
        'langC2S': [],
        'langS2C': [],
    }

    # region: Helper Methods

    def get_S2C_mac_hasher_and_size(self) -> Tuple[hashes.HashAlgorithm, int]:
        return SSH_User.server_supported_algos['macS2C'][self.accepted_algo['macS2C']]

    def get_C2S_mac_hasher_and_size(self) -> Tuple[hashes.HashAlgorithm, int]:
        return SSH_User.server_supported_algos['macC2S'][self.accepted_algo['macC2S']]

    def get_kex_hasher(self) -> hashes.HashAlgorithm:
        return SSH_User.server_supported_algos['kex'][self.accepted_algo['kex']][1]

    def get_host_key_hasher(self) -> hashes.HashAlgorithm:
        return SSH_User.server_supported_algos['server-host-key'][self.accepted_algo['server-host-key']][0]

    def get_host_key(self) -> PrivateKey:
        return PrivateKey(SSH_User.server_supported_algos['server-host-key'][self.accepted_algo['server-host-key']][1])

    def get_kex(self) -> Union[Callable[[], x25519.X25519PrivateKey], Callable[[], x448.X448PrivateKey], dh.DHParameters]:
        return SSH_User.server_supported_algos['kex'][self.accepted_algo['kex']][0]

    def get_S2C_cipher(self) -> CipherWrapper:
        tup = SSH_User.server_supported_algos['encryptS2C'][self.accepted_algo['encryptS2C']]

        key: bytes = self.get_big_iv(KeyType.ENCRYPTION_SERVER, tup[2] // 8)
        iv: bytes = self.get_big_iv(KeyType.IV_SERVER, 16)

        assert len(key) == (tup[2] // 8)

        return CipherWrapper(Cipher(tup[0](key), mode=tup[1](iv)))

    def get_C2S_cipher(self) -> Cipher:
        tup = SSH_User.server_supported_algos['encryptC2S'][self.accepted_algo['encryptC2S']]

        key: bytes = self.get_big_iv(KeyType.ENCRYPTION_CLIENT, tup[2] // 8)
        iv: bytes = self.get_big_iv(KeyType.IV_CLIENT, 16)

        assert len(key) == tup[2] // 8

        return CipherWrapper(Cipher(tup[0](key), mode=tup[1](iv)))

    def get_cipher_block_size(self) -> int:
        return SSH_User.server_supported_algos['encryptS2C'][self.accepted_algo['encryptS2C']][2] // 8
    # endregion

    # Ordered list of arguments for kex synthisizing
    KEX_INIT_KEYS: List[str] = ('kex', 'server-host-key', 'encryptC2S', 'encryptS2C', 'macC2S', 'macS2C', 'compressionC2S', 'compressionS2C', 'langC2S', 'langS2C',)

    def recv(self, length=4096) -> Tuple[Packet, Stage]:
        recv = self.socket.recv(length)
        self.incrementSeq_client()
        packet = Packet(recv, not self.C2S_cipher)
        stage = Stage.getStage(recv)
        if self.C2S_cipher:
            packet.decrypt(self.C2S_cipher, self.get_C2S_mac_hasher_and_size())
            stage = Stage.getStage(packet._data.getvalue())
            if packet._get_length() == 0:
                raise DecryptionError('No data received')
            size_field = packet.getuint32()
            packet._reset_index()
            if size_field > 35000:
                tup = SSH_User.server_supported_algos['encryptS2C'][self.accepted_algo['encryptS2C']]
                text = 'Packet to large! Likely Packet failed to decrypt.'
                _log.add_line(f'key: {self.get_big_iv(KeyType.ENCRYPTION_SERVER, tup[2] // 8)}. nonce: {self.get_big_iv(KeyType.IV_SERVER, 16)}')
                _log.add_line(text)
                _log.log(packet, self.socket, stage)
                self.send_disconnect(reason=text)
                raise DecryptionError(text)
            if size_field != packet._get_length() - 4:
                text = 'Decryption Failed.'
                _log.add_line(text)
                _log.log(packet, self.socket, stage)
                self.send_disconnect(reason=text)
                raise DecryptionError(text)
            packet.strip()
            hmac = packet.compute_mac(self.proto_mac_client(), self.get_C2S_mac_hasher_and_size()[0])
            if hmac != packet.mac:
                _log.add_line('Invalid Mac.')
                _log.add_line(f'Excepted length: {self.get_C2S_mac_hasher_and_size()[1]}')
                _log.add_line(f'Computed length: {len(hmac)}')
                _log.add_line(f'Computed MAC: {str(hmac)}')
                _log.add_line(f'Received length: {len(packet.mac)}')
                _log.add_line(f'Received MAC: {str(packet.mac)}')
                self.send_disconnect(DISCONNECT_CODE.SSH_DISCONNECT_MAC_ERROR.value, 'Mac is incorrect.')
                raise MessageAuthenticationCodeError('Received Invalid Mac')
        _log.log(packet, self.socket, stage if stage != Stage.UNKNOWN else FakeStage('UNKNOWN', packet.getByte()))
        packet._reset_index()
        return (packet, stage)

    def send(self, packet: PacketBuilder):
        if not packet.packeted:
            packet.packetize(padding_modulo=(8 if not self.S2C_cipher else self.get_cipher_block_size()))
        if self.S2C_cipher:
            packet.encryptAndAddMac(self.S2C_cipher, self.proto_mac_server(), self.get_S2C_mac_hasher_and_size()[0])
        _log.log(packet, self.socket, stage=Stage.getStage(packet._data))
        self.socket.send(packet.data())
        if self.needs_rekey:
            self.needs_rekey = False
            self.update_ciphers()
        self.incrementSeq_server()

    def send_disconnect(self, reasoncode: int = 10, reason: str = 'Reasons'):
        self.send(SSH_User.synthesize_disconnect(reasoncode, reason))

    def __init__(self, socket: sock.socket):
        self.socket: sock.socket = socket
        self.socket.settimeout(10.0)
        self.algo_bundle: Dict[str, List[str]] = None
        self.accepted_algo: Dict[str, str] = None

        self.client_kex_key: bytes = None
        self.client_id_string: bytes = None
        self.client_KEXINIT_payload: bytes = None
        self.server_KEXINIT_payload: bytes = None

        self._sequenceNumber_server: int = -1
        self._sequenceNumber_client: int = -1

        '''rfc4253 6.3 - sequence number will be initlized to 0 for the first packet(HEADER)'''
        self.shared_secret: int = None
        self.S2C_cipher: CipherWrapper = None
        self.C2S_cipher: CipherWrapper = None
        self.H: bytes = None
        self.session_id: bytes = None
        self.requested_service: str = None
        self.username: str = None
        self.password: str = None
        self.needs_rekey: bool = False
        '''rfc4253 7.1 cookie will be used as Session identifier'''

    def incrementSeq_server(self):
        self._sequenceNumber_server += 1
        self._sequenceNumber_server &= 0xFFFF_FFFF  # bit manipulation for uint32

    def incrementSeq_client(self):
        self._sequenceNumber_client += 1
        self._sequenceNumber_client &= 0xFFFF_FFFF  # bit manipulation for uint32

    def update_ciphers(self):
        self.S2C_cipher = self.get_S2C_cipher()
        self.C2S_cipher = self.get_C2S_cipher()

    def bootstrap(self, data: bytes, user: User):
        try:
            stage: Stage = Stage.getStage(data)
            _log.log(Packet(data, True), self.socket, stage)
            packet: Packet = Packet(data, False)
            logging.info(self.socket.getpeername())
            while True:
                logging.debug(f'~~~{stage.name}~~~')
                accept(stage, self, packet)
                packet, stage = self.recv()
            _log.add_line('"Unknown Error has occured."')
            self.send_disconnect(reason="Unknown Error has occured.")
            self.socket.close()
        except ConnectionAbortedError:
            print('ConnectionAbortedError')
        except ConnectionResetError:
            print("ConnectionResetError")
        except OSError:
            print(traceback.format_exc())
        except (MessageAuthenticationCodeError, DecryptionError, NoMatchingAlgorithms) as e:
            print(f'{type(e).__name__}: {e}')
        except sock.timeout:
            print('Timeout')
        finally:
            # print(traceback.format_exc())
            try:
                self.send_disconnect(reason='Unknown Exception.')
            except:  # noqa: E722
                pass
            _log.write()

    def proto_mac_server(self) -> Tuple[bytes, int]:
        '''
        Tuple[INTEGRITY_KEY, share_secret, sequence_number]
        RFC 4253 Section 6.4
        mac = MAC(key, sequence_number || unencrypted_packet)
        '''
        return (
            self.get_big_iv(KeyType.INTEGRITY_SERVER, self.get_S2C_mac_hasher_and_size()[1]),
            self._sequenceNumber_server)

    def proto_mac_client(self) -> Tuple[bytes, int]:
        return (
            self.get_big_iv(KeyType.INTEGRITY_CLIENT, self.get_C2S_mac_hasher_and_size()[1]),
            self._sequenceNumber_client)

    def get_big_iv(self, type: 'KeyType', byte_size: int) -> bytes:
        '''RFC 4253 Section 7.2'''
        checkType(type, KeyType)
        total = k = PacketBuilder(
            ).addmpint(self.shared_secret
            )._addBytes(self.H
            )._addByte(type.value
            )._addBytes(self.session_id
            ).hash(self.get_kex_hasher())

        while len(total) < byte_size:
            k = PacketBuilder(
                ).addmpint(self.shared_secret
                )._addBytes(self.H
                )._addBytes(k
                ).hash(self.get_kex_hasher())
            total += k
        return total[:byte_size]


handles: Dict[Stage, Dict[str, Callable]] = {}

_log: logger = _logger


def registerStage(stage: Stage, parser: Callable[['SSH_User', Packet], None], response: Callable[['SSH_User'], PacketBuilder], extra_args: Dict[str, Any] = None):
    handles[stage] = {
        "parser": parser,
        "response": response,
        "extra": extra_args
    }


def accept(stage: Stage, user: 'SSH_User', packet: Packet) -> None:
    if stage not in handles:
        _log.add_line('Unaccounted for Packet type received.')
    obj: Dict[str, Callable] = handles[stage]
    obj['parser'](user, packet)
    if obj['extra']:
        user.send(obj['response'](user, **obj['extra']))
    else:
        user.send(obj['response'](user))


def parse_kex_init(self: 'SSH_User', packet: Packet) -> None:
    '''
    byte         SSH_MSG_KEXINIT
    byte[16]     cookie (random bytes)
    name-list    kex_algorithms
    name-list    server_host_key_algorithms
    name-list    encryption_algorithms_client_to_server
    name-list    encryption_algorithms_server_to_client
    name-list    mac_algorithms_client_to_server
    name-list    mac_algorithms_server_to_client
    name-list    compression_algorithms_client_to_server
    name-list    compression_algorithms_server_to_client
    name-list    languages_client_to_server
    name-list    languages_server_to_client
    boolean      first_kex_packet_follows
    uint32       0 (reserved for future extension)
    '''
    from objs.SSH_User import SSH_User  # noqa: E402
    self.client_KEXINIT_payload = packet._data.getvalue()
    assert packet.getByte() == Stage.KEX_INIT.value

    packet._get(16)  # cookie

    algo_bundle = {}
    for x in SSH_User.KEX_INIT_KEYS:
        algo_bundle[x] = packet.getNameList()

    self.algo_bundle = algo_bundle
    self.accepted_algo = {}


def parse_specific_kex_init(self: 'SSH_User', packet: Packet):
    '''
    byte      SSH_MSG_KEXDH_INIT
    mpint     e
    '''
    assert packet.getByte() == Stage.SPECIFIC_KEX_INIT.value
    self.client_kex_key = packet.getBytes()
    # TODO: Verify it's a valid key


def parse_service_request(self: 'SSH_User', packet: Packet) -> str:
    '''
    byte      SSH_MSG_SERVICE_REQUEST
    string    service name
    '''
    assert packet.getByte() == Stage.SERVICE_REQUEST.value
    self.requested_service = packet.getString()


def parse_channel_open(self: 'SSH_User', packet: Packet) -> str:
    '''
    byte      SSH_MSG_CHANNEL_OPEN
    string    channel type in US-ASCII only
    uint32    sender channel
    uint32    initial window size
    uint32    maximum packet size
    ....      channel type specific data follows
    '''
    assert packet.getByte() == Stage.CHANNEL_OPEN.value
    channel_type = packet.getString()
    if channel_type != 'session':
        self.send_disconnect(reason=f'{channel_type} is not supported.')
    sender_channel = packet.getuint32()
    init_window_size = packet.getuint32()
    max_packet_size = packet.getuint32()
    return


def parse_header(self: 'SSH_User', packet: Packet):
    self.client_id_string = packet._data.getvalue()


def parse_disconnect(self: 'SSH_User', packet: Packet):
    '''
    byte      SSH_MSG_DISCONNECT
    uint32    reason code
    string    description in ISO-10646 UTF-8 encoding [RFC3629]
    string    language tag [RFC3066]
    '''
    assert packet.getByte() == Stage.DISCONNECT.value
    _log.add_line(f'Reason code: {packet.getuint32()}\nReason: {packet.getString()}\nLang: {packet.getString()}')


# @staticmethod
def mpint_to_bytes(val: int) -> bytes:
    checkType(val, int)
    bitlen = val.bit_length()

    val = val.to_bytes(bitlen + 7, 'big', signed=False)
    val = PacketBuilder.remove_zero_padding(val)

    if (bitlen % 8) == 0:    # FIXME: Gross haxs TODO: Why?
        val = b'\x00' + val
    elif (bitlen % 4) == 4:  # FIXME: Gross haxs TODO: Why?
        val = val[2:]
    return val


# @staticmethod
def mpint_to_int(data: bytes) -> int:
    checkType(data, bytes)
    return int.from_bytes(data, 'big', signed=False)


# region: PacketBuilders
@staticmethod
def synthesize_disconnect(disconnect_code: int = 10, reason: str = 'Not Implemented', lang: str = 'us-en') -> PacketBuilder:
    return PacketBuilder()._addByte(0x01)._adduint32(disconnect_code).addString(reason).addString(lang)


def synthesize_header(self: 'SSH_User') -> PacketBuilder:
    builder = PacketBuilder()._addBytes(self.server_id_string)
    builder.packeted = True
    return builder


def synthesize_ignore(self: 'SSH_User', msg: str = 'Hi! I\'m empty.') -> PacketBuilder:
    return PacketBuilder()._addByte(0x02).addString(msg)


def synthesize_user_auth_success(self: 'SSH_User') -> PacketBuilder:
    return PacketBuilder()._addByte(Stage.USER_AUTH_SUCCESS.value)


def synthesize_unimplemented(self: 'SSH_User') -> PacketBuilder:
    return PacketBuilder()._addByte(0x03)._adduint32(self._sequenceNumber_client)


def synthesize_newkeys(self: 'SSH_User') -> PacketBuilder:
    self.needs_rekey = True
    return PacketBuilder()._addByte(21)


def synthesize_service_accept(self: 'SSH_User') -> PacketBuilder:
    if self.requested_service in ['ssh-userauth']:
        return PacketBuilder()._addByte(0x06).addString(self.requested_service)
    else:
        self.send_disconnect(f'Service {self.requested_service} is not Supported.')
        return None


def synthesize_debug(self: 'SSH_User', always_display: bool = True, msg: str = 'Run. Fast.', lang: str = 'us-en') -> PacketBuilder:
    return PacketBuilder(
        )._addByte(0x04
        )._addBoolean(always_display
        ).addString(msg
        ).addString(lang)


def synthesize_kex_init(self: 'SSH_User', first_kex_packet_follows: bool = True) -> PacketBuilder:
    builder = PacketBuilder()._addByte(0x14)
    builder._addBytes(os.urandom(16))
    for algo_type in SSH_User.KEX_INIT_KEYS:
        sections_size = builder.len()

        supported = SSH_User.server_supported_algos[algo_type]
        if isinstance(supported, dict):
            supported = list(supported.keys())

        if len(self.algo_bundle[algo_type]) == 0 or len(supported) == 0:  # Janky
            builder.addString(b'')
        for algo in supported:
            if algo in self.algo_bundle[algo_type]:
                builder.addString(algo)
                self.accepted_algo[algo_type] = algo
                break
            pass
        if sections_size == builder.len():
            _log.add_line('Client and Server don\'t have a matching %s' % algo_type)
            _log.add_line(f'Supported: {supported}')
            _log.add_line(f'Offered: {self.algo_bundle[algo_type]}')
            self.send_disconnect(14, 'No Matching %s. %s' % (algo_type, supported))
            raise NoMatchingAlgorithms('No Matching %s. %s' % (algo_type, supported))

    builder._addBoolean(first_kex_packet_follows)._adduint32(0)  # Reserved

    _log.add_line(f'Client Supported Algos: {json.dumps(self.algo_bundle, indent=4)}')
    _log.add_line(f'Accepted: {json.dumps(self.accepted_algo,indent=4)}')
    self.algo_bundle = None
    self.server_KEXINIT_payload = builder.data()
    return builder


def synthesize_kex_dh(self: 'SSH_User') -> 'PacketBuilder':
    dhpara: dh.DHParameters = self.get_kex()
    kex_key = dhpara.generate_private_key()
    f = kex_key.public_key().public_numbers().y
    client_dh = dh.DHPublicNumbers(self.mpint_to_int(self.client_kex_key), dhpara.parameter_numbers())
    self.shared_secret = self.mpint_to_int(kex_key.exchange(client_dh.public_key()))

    host_key: PrivateKey = self.get_host_key()

    builder = PacketBuilder(
        )._addByte(31
        ).addString(host_key.getPublicKeyBytes()
        ).addmpint(f)

    self.H = PacketBuilder(
        ).addString(self.client_id_string[:-2]
        ).addString(self.server_id_string[:-2]
        ).addString(self.client_KEXINIT_payload
        ).addString(self.server_KEXINIT_payload
        ).addString(host_key.getPublicKeyBytes()
        ).addmpint(self.mpint_to_int(self.client_kex_key)
        ).addmpint(f
        ).addmpint(self.shared_secret).hash(self.get_kex_hasher())
    if not self.session_id:
        self.session_id = self.H
    sig = host_key.sign(self.accepted_algo['server-host-key'], self.H, self.get_host_key_hasher())
    builder.addString(sig)
    return builder


def synthesize_kex_ecdh(self: 'SSH_User') -> PacketBuilder:
    key: Union[x25519.X25519PrivateKey, x448.X448PrivateKey] = self.get_kex()()
    server_public_key = key.public_key().public_bytes(Encoding.Raw, PublicFormat.Raw)

    client_public_key: Union[x25519.X25519PublicKey, x448.X448PublicKey] = None
    client_public_key = (x25519.X25519PublicKey if isinstance(key, x25519.X25519PrivateKey) else x448.X448PublicKey).from_public_bytes(self.client_kex_key)

    self.shared_secret = self.mpint_to_int(key.exchange(client_public_key))

    host_key: PrivateKey = self.get_host_key()
    host_public_bytes = host_key.getPublicKeyBytes()

    builder = PacketBuilder(
        )._addByte(31
        ).addString(host_public_bytes
        ).addString(server_public_key)

    self.H = PacketBuilder(
        ).addString(self.client_id_string[:-2]
        ).addString(self.server_id_string[:-2]
        ).addString(self.client_KEXINIT_payload
        ).addString(self.server_KEXINIT_payload
        ).addString(host_public_bytes
        ).addString(self.client_kex_key
        ).addString(server_public_key
        ).addmpint(self.shared_secret).hash(self.get_kex_hasher())
    if not self.session_id:
        self.session_id = self.H

    sig = host_key.sign(self.accepted_algo['server-host-key'], self.H, self.get_host_key_hasher())
    builder.addString(sig)

    return builder
# endregion


def parse_user_auth_request(self: 'SSH_User', packet: Packet):
    '''
    byte      SSH_MSG_USERAUTH_REQUEST
    string    user name in ISO-10646 UTF-8 encoding [RFC3629]
    string    service name in US-ASCII
    string    method name in US-ASCII
    ....      method specific fields
        string    "publickey"
        boolean   TRUE (signature follows)
        string    public key algorithm name
        string    public key to be used for authentication
        optional[str] signature of:
            string    session identifier
            byte      SSH_MSG_USERAUTH_REQUEST
            string    user name
            string    service name
            string    "publickey"
            boolean   TRUE
            string    public key algorithm name
            string    public key to be used for authentication
        ||
        string    "password"
        boolean   FALSE (is a passwrod change request and new password follows)
        string    plaintext password in ISO-10646 UTF-8 encoding [RFC3629]
        optional[str] new password
        ||
        string    "hostbased"
        string    public key algorithm for host key
        string    public host key and certificates for client host
        string    client host name expressed as the FQDN in US-ASCII
        string    user name on the client host in ISO-10646 UTF-8 encoding
        [RFC3629]
        string    signature of:
            string    session identifier
            byte      SSH_MSG_USERAUTH_REQUEST
            string    user name
            string    service name
            string    "hostbased"
            string    public key algorithm for host key
            string    public host key and certificates for client host
            string    client host name expressed as the FQDN in US-ASCII
            string    user name on the client host in ISO-10646 UTF-8 encoding [RFC3629]
    '''
    assert packet.getByte() == Stage.USER_AUTH_REQUEST.value
    self.username = packet.getString()


kexer: Dict[str, Callable[['SSH_User'], PacketBuilder]] = {
    'diffie': synthesize_kex_dh,  # synthesize_kexdh_reply
    'curve25519': synthesize_kex_ecdh,
    'curve448': synthesize_kex_ecdh,
}


def get_kex_packet(self: 'SSH_User') -> PacketBuilder:
    return kexer[self.accepted_algo['kex'].split('-')[0]](self)


nothing_parser: Callable[['SSH_User', Packet], None] = (lambda user, packet: None)
nothing_response: Callable[['SSH_User'], PacketBuilder] = (lambda user: None)

registerStage(Stage.HEADER, parse_header, synthesize_header)
registerStage(Stage.EMPTY, nothing_parser, synthesize_disconnect, extra_args={'reason': 'Received Empty Packet'})
registerStage(Stage.DISCONNECT, parse_disconnect, nothing_response)
registerStage(Stage.UNKNOWN, nothing_parser, synthesize_unimplemented)
registerStage(Stage.KEX_INIT, parse_kex_init, synthesize_kex_init)
registerStage(Stage.SPECIFIC_KEX_INIT, parse_specific_kex_init, get_kex_packet)
registerStage(Stage.NEWKEYS, nothing_parser, synthesize_newkeys)
registerStage(Stage.SERVICE_REQUEST, parse_service_request, synthesize_service_accept)
registerStage(Stage.USER_AUTH_REQUEST, nothing_parser, synthesize_user_auth_success)
registerStage(Stage.CHANNEL_OPEN, parse_channel_open, nothing_response)

# EOF
